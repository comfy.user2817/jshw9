

"use strict"



//1


const feat = document.querySelectorAll('.feature');
console.log(feat)

feat.forEach(featStyles => {
    featStyles.style.textAlign = 'center';   
});


//Або 


const featTwo = document.querySelector('.feature');

featTwo.style.textAlign = 'center';



//2


// const h2Elements = document.querySelectorAll('h2');

// h2Elements.forEach((element) => {
//   element.textContent = 'Awesome feature';
// });
const exTwo = document.querySelectorAll('h2');
// console.log(exTwo)

exTwo.forEach(element => {
    element.textContent = "Awesome feature";
});



//3


let exThre = document.querySelectorAll('.feature-title');

exThre.forEach(element => {
    element.textContent += '!'
})